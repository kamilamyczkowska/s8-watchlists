package com.kameocode.watchlists;

import com.kameocode.watchlists.domain.watchlists.am.AmWatchlistEntry;
import com.kameocode.watchlists.domain.watchlists.deny.DenyWatchlistEntry;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.ConnectionCallback;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.StreamUtils;

import javax.sql.DataSource;
import java.io.IOException;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)

@TestPropertySource(locations = {"classpath:application.yml"},
        properties = {"inputFile = watchlist-test-data-invalid.csv", "watchlistType = DENY"})
public class DenyInvalidDataTest {


    @Autowired
    private JobLauncherTestUtils jobLauncherTestUtils;

    @Autowired
    private JdbcTemplate jdbcTemplate;


    @Test
    public void testInvalidDenyImport()
            throws Exception {
        // given: inputFile contains incorrect data for that watchlist
        // given: table exists and has one record
        jdbcTemplate.execute("CREATE TABLE WATCHLIST_ENTRY_DENY (\n" +
                        "  id     BIGINT NOT NULL PRIMARY KEY,\n" +
                        "  first_name    VARCHAR(255),\n" +
                        "  second_name   VARCHAR(255),\n" +
                        "  surname       VARCHAR(255),\n" +
                        "  date_of_birth DATE\n" +
                        ");");
        jdbcTemplate.update(
                "INSERT INTO WATCHLIST_ENTRY_DENY (id, first_name) VALUES (?, ?)",
                5, "zzz"
        );


        // when: the job is launched
        JobExecution jobExecution = jobLauncherTestUtils.launchJob();

        // then: data is not imported to table
        long count = jdbcTemplate.queryForObject("select count(*) from WATCHLIST_ENTRY_DENY", Long.class);
        assertEquals(1, count);

        // and:
        assertEquals(ExitStatus.FAILED, jobExecution.getExitStatus());
    }
}