package com.kameocode.watchlists;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)

@TestPropertySource(locations = {"classpath:application.yml"},
        properties = {"inputFile = watchlist-test-data.csv", "watchlistType = DENY"})
public class DenyTest {


    @Autowired
    private JobLauncherTestUtils jobLauncherTestUtils;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    public void testCorrectDenyImport()
            throws Exception {
        // given: inputFile contains correct data for that watchlist

        // when:  the job is launched
        JobExecution jobExecution = jobLauncherTestUtils.launchJob();

        // then: data is imported to table
        long count = jdbcTemplate.queryForObject("select count(*) from WATCHLIST_ENTRY_DENY", Long.class);
        assertEquals(3, count);
        // and: status is completed
        assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());

    }
}