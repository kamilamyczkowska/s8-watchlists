package com.kameocode.watchlists;


import com.kameocode.watchlists.common.CommonItemProcessor;
import com.kameocode.watchlists.common.CommonJobExecutionListener;
import com.kameocode.watchlists.domain.WatchlistType;
import com.kameocode.watchlists.domain.watchlists.am.AmWatchlistEntry;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.util.StreamUtils;

import javax.sql.DataSource;
import java.io.IOException;
import java.nio.charset.Charset;

@Slf4j
@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

    @Value("${watchlistType}")
    private WatchlistType watchlistType;

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Value("${chunkSizeForImport}")
    private int chunkSizeForImport;


    @Value("${chunkSizeForValidation}")
    private int chunkSizeForValidation;


    @Bean
    public Resource inputResource(@Value("${inputFile}") String inputFile) {
        if (inputFile == null) {
            throw new IllegalArgumentException("inputFile property is required");
        }
        FileSystemResource resource = new FileSystemResource(inputFile);
        if (!resource.exists()) {
            log.warn("Resource {} doesn't exist, searching in the classpath", inputFile);
            return new ClassPathResource(inputFile);
        }
        return resource;
    }


    @Bean
    public Job importWatchlist(CommonJobExecutionListener listener, Step stepDataIsValid, Step stepWriteToDatabase) {
        return jobBuilderFactory.get("importWatchlistJob_" + watchlistType.name())
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(stepDataIsValid)
                .next(stepWriteToDatabase)
                .end()
                .build();
    }

    @Bean
    public Step stepDataIsValid(FlatFileItemReader reader, ItemProcessor processor) {
        return stepBuilderFactory.get("stepDataIsValid")
                .chunk(chunkSizeForValidation)
                .reader(reader)
                .processor(new CommonItemProcessor(processor))
                .build();
    }


    @Bean
    public Step stepWriteToDatabase(FlatFileItemReader reader, ItemProcessor processor, ItemWriter writer) {
        return stepBuilderFactory.get("stepWriteToDatabase")
                .chunk(chunkSizeForImport)
                .reader(reader)
                .processor(processor)
                .writer(writer)
                .build();
    }

    @Bean
    public FlatFileItemReader reader(Resource resource) {
        return new FlatFileItemReaderBuilder()
                .name("watchlistEntryReader")
                .resource(resource)
                .delimited()
                .names(new String[]{
                        "id",
                        "name1",
                        "secondName1",
                        "surname1",
                        "name2",
                        "secondName2",
                        "surname2",
                        "placeOfBirth",
                        "dateOfBirth_1",
                        "dateOfBirth_2",
                        "dateOfBirth_3",
                })
                .fieldSetMapper(new BeanWrapperFieldSetMapper() {{
                    setTargetType(watchlistType.getRawEntryClass());
                    setDistanceLimit(0);
                    setStrict(false);
                }})
                .build();
    }


    @Autowired
    private WatchlistsConfiguration watchlistsConfiguration;

    @Bean
    public JdbcBatchItemWriter<AmWatchlistEntry> writer(DataSource dataSource) throws IOException {
        String sql = StreamUtils.copyToString(watchlistsConfiguration.getInsertTableScript()
                .getInputStream(), Charset.defaultCharset());
        return new JdbcBatchItemWriterBuilder<AmWatchlistEntry>()
                .itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>())
                .sql(sql)
                .dataSource(dataSource)
                .build();
    }

}
