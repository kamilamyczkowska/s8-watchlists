package com.kameocode.watchlists;

import com.kameocode.watchlists.domain.WatchlistType;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
@EnableConfigurationProperties
@ConfigurationProperties("params")
@Data
public class WatchlistsConfiguration {

    private HashMap<WatchlistType, WatchlistConfig> watchlists;

    @Value("${watchlistType}")
    private WatchlistType watchlistType;

    public WatchlistConfig get() {
        return watchlists.get(watchlistType);
    }

    public Resource getCreateTableScript() {
        return new ClassPathResource(get().getSqlCreateScriptPath());
    }

    public Resource getInsertTableScript() {
        return new ClassPathResource(get().getSqlInsertScriptPath());
    }

    public String getTargetTableName() {
        return get().getTargetTableName();
    }

    public String getTemporaryTableName() {
        return get().getTemporaryTableName();
    }

    @Data
    private static class WatchlistConfig {
        private String temporaryTableName;
        private String targetTableName;
        private String sqlCreateScriptPath;
        private String sqlInsertScriptPath;
    }


}
