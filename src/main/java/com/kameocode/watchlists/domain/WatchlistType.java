package com.kameocode.watchlists.domain;

import com.kameocode.watchlists.domain.watchlists.am.AmRawEntry;
import com.kameocode.watchlists.domain.watchlists.deny.DenyRawEntry;
import com.kameocode.watchlists.domain.watchlists.pep.PepRawEntry;

public enum WatchlistType {
    AM(AmRawEntry.class),
    PEP(PepRawEntry.class),
    DENY(DenyRawEntry.class);

    private final Class rawEntryClass;

    WatchlistType(Class rawEntryClass) {
        this.rawEntryClass = rawEntryClass;
    }

    public Class getRawEntryClass() {
        return rawEntryClass;
    }
}
