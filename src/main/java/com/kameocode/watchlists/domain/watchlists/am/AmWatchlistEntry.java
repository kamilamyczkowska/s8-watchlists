package com.kameocode.watchlists.domain.watchlists.am;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;

import java.time.LocalDate;

@Builder
@ToString
@Value
public class AmWatchlistEntry {
    private final long id;
    private final String placeOfBirth;
    private final LocalDate dateOfBirth;
}
