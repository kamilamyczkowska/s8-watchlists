package com.kameocode.watchlists.domain.watchlists.deny;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;

import java.time.LocalDate;

@ToString
@Builder
@Value
public class DenyWatchlistEntry {
    private final long id;
    private final String firstName;
    private final String secondName;
    private final String surname;
    private final LocalDate dateOfBirth;
}
