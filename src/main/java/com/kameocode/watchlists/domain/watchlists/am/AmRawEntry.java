package com.kameocode.watchlists.domain.watchlists.am;


import lombok.Data;

@Data
public class AmRawEntry {

    private String id;
    private String placeOfBirth;
    private String dateOfBirth_3;


}
