package com.kameocode.watchlists.domain.watchlists.deny;

import lombok.Data;

@Data
public class DenyRawEntry {

    private String id;

    private String name1;
    private String secondName1;
    private String surname1;
    private String dateOfBirth_1;

}
