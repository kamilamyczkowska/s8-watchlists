package com.kameocode.watchlists.domain.watchlists.pep;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;

import java.time.LocalDate;

@ToString
@Builder
@Value
public class PepWatchlistEntry {
    private final long id;
    private final String firstName;
    private final String secondName;
    private final String surname;
    private final LocalDate dateOfBirth;
}
