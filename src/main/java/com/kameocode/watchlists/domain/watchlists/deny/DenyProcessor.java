package com.kameocode.watchlists.domain.watchlists.deny;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
@ConditionalOnProperty(name = "watchlistType", havingValue = "DENY")
@Slf4j
public class DenyProcessor implements ItemProcessor<DenyRawEntry, DenyWatchlistEntry> {

    @Override
    public DenyWatchlistEntry process(final DenyRawEntry entry) {

        final DenyWatchlistEntry transformedEntry = DenyWatchlistEntry
                .builder()
                .id(Long.valueOf(entry.getId()))
                .firstName(entry.getName1())
                .secondName(entry.getSecondName1())
                .surname(entry.getSurname1())
                .dateOfBirth(LocalDate.parse(entry.getDateOfBirth_1()))
                .build();
        log.info("Converting (" + entry + ") into (" + transformedEntry + ")");
        return transformedEntry;
    }


}