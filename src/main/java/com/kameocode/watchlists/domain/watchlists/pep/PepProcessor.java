package com.kameocode.watchlists.domain.watchlists.pep;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.time.LocalDate;


@Component
@ConditionalOnProperty(name = "watchlistType", havingValue = "PEP")
@Slf4j
public class PepProcessor implements ItemProcessor<PepRawEntry, PepWatchlistEntry> {

    @Override
    public PepWatchlistEntry process(final PepRawEntry entry) {

        final PepWatchlistEntry transformedEntry = PepWatchlistEntry
                .builder()
                .id(Long.valueOf(entry.getId()))
                .firstName(entry.getName2())
                .secondName(entry.getSecondName2())
                .surname(entry.getSurname2())
                .dateOfBirth(LocalDate.parse(entry.getDateOfBirth_2()))
                .build();
        log.info("Converting (" + entry + ") into (" + transformedEntry + ")");
        return transformedEntry;
    }


}