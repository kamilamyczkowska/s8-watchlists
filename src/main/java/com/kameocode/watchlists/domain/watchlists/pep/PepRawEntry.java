package com.kameocode.watchlists.domain.watchlists.pep;

import lombok.Data;

@Data
public class PepRawEntry {

    private String id;

    private String name2;
    private String secondName2;
    private String surname2;
    private String dateOfBirth_2;

}
