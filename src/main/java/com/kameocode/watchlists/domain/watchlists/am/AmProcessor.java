package com.kameocode.watchlists.domain.watchlists.am;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
@ConditionalOnProperty(name = "watchlistType", havingValue = "AM")
@Slf4j
public class AmProcessor implements ItemProcessor<AmRawEntry, AmWatchlistEntry> {

    @Override
    public AmWatchlistEntry process(final AmRawEntry entry) {

        final AmWatchlistEntry transformedEntry = AmWatchlistEntry
                .builder()
                .id(Long.valueOf(entry.getId()))
                .placeOfBirth(entry.getPlaceOfBirth())
                .dateOfBirth(LocalDate.parse(entry.getDateOfBirth_3()))
                .build();

        log.info("Converting (" + entry + ") into (" + transformedEntry + ")");

        return transformedEntry;
    }


}