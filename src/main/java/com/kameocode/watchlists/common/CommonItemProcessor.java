package com.kameocode.watchlists.common;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;

/**
 * this class logs errors
 */
@Slf4j
@RequiredArgsConstructor
public class CommonItemProcessor<T1, T2> implements ItemProcessor<T1, T2> {
    private final ItemProcessor<T1, T2> processor;

    @Override
    public T2 process(T1 t1) throws Exception {
        try {
            return processor.process(t1);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            throw ex;
        }
    }
}
