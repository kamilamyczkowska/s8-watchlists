package com.kameocode.watchlists.common;

import com.kameocode.watchlists.WatchlistsConfiguration;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.jdbc.core.ConnectionCallback;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;

import static java.lang.String.format;

@Slf4j
@Component
@RequiredArgsConstructor
public class CommonJobExecutionListener extends JobExecutionListenerSupport {

    private final JdbcTemplate jdbcTemplate;

    private final WatchlistsConfiguration configuration;


    @Override
    public void beforeJob(JobExecution jobExecution) {
        String temporaryTableName = configuration.getTemporaryTableName();
        if (checkIfTableExists(temporaryTableName)) {
            throw new IllegalStateException(format("Table %s already exists", temporaryTableName));
        }

        jdbcTemplate.execute((ConnectionCallback<Void>) connection -> {
            ScriptUtils.executeSqlScript(connection, configuration.getCreateTableScript());
            return null;
        });
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        String targetTableName = configuration.getTargetTableName();
        String temporaryTableName = configuration.getTemporaryTableName();

        if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
            log.info("Job {} completed successfully", jobExecution.toString());
            String backupTableName = targetTableName + "_backup";

            if (checkIfTableExists(targetTableName)) {
                log.trace("Target table {} already exists", targetTableName);
                jdbcTemplate.update(format("ALTER TABLE %s RENAME TO %s", targetTableName, backupTableName));
                jdbcTemplate.update(format("ALTER TABLE %s RENAME TO %s", temporaryTableName, targetTableName));
                jdbcTemplate.update(format("DROP TABLE %s", backupTableName));
            } else {
                log.warn("Target table {} doesn't exist", targetTableName);
                jdbcTemplate.update(format("ALTER TABLE %s RENAME TO %s", temporaryTableName, targetTableName));
            }

        } else if (jobExecution.getStatus() == BatchStatus.FAILED) {
            log.info("Job {} failed", jobExecution.toString());
            if (checkIfTableExists(temporaryTableName)) {
                jdbcTemplate.update(format("DROP TABLE %s", temporaryTableName));
            }
        }
    }

    private boolean checkIfTableExists(String tableName) {
        return jdbcTemplate.execute((ConnectionCallback<Boolean>) connection -> {
            ResultSet tables = connection.getMetaData().getTables(null, null, tableName, null);
            return tables.next();
        });
    }
}