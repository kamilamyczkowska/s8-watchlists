
CREATE TABLE WATCHLIST_ENTRY_DENY_TEMPORARY (
  id     BIGINT NOT NULL PRIMARY KEY,
  first_name    VARCHAR(255),
  second_name   VARCHAR(255),
  surname       VARCHAR(255),
  date_of_birth DATE
);


